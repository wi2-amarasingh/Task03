import {Article} from "Article.js";
export const articles:Article[] = [
    new Article("0","First Article", "Here is the main body for the first article"),
    new Article("1", "Second Article", "Second article: Here is the main body")
];

