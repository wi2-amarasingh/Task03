import {articles} from "./ArticleData.js";
import {Article} from "./Article.js";

// This is the HTML template to use for articles
let templateHTML:string = 
`
<article>
    <h1>{head}</h1>
    <p>{body}</p>
</article>
`;  

// This function selects an article for display
var selectItem = function(e:Event){
    // Which nav item was clicked?
    let articleId:string = e.srcElement.id;

    // Get a reference to the main element
    let main = <HTMLElement> document.querySelector("main");

    //Get the selected article data
    let currentArticle:Article = articles[articleId];

    //Fill out the template with the data from the article
    let parsedHTML = templateHTML.replace("{head}", currentArticle.title).replace("{body}",currentArticle.text);
    
    //assign it to the main element in the HTML
    main.innerHTML = parsedHTML;
}

// Get a reference to the navigation element
let nav = <HTMLElement> document.querySelector("nav");

// Create a list for the navigation items
let ol = document.createElement("ol");

// loop through the nav. items and create a list item for each
for(let item of articles){
    let li = document.createElement("li");
    li.id = item.id;
    li.innerText = item.title;
    li.addEventListener("click", selectItem);
    ol.appendChild(li);
}

// Add our list to the nav element in the HTML
nav.appendChild(ol);

 





